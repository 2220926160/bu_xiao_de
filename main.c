#include<stdio.h>
#include<time.h>

#include "hashtable.h"

int  main() {
    clock_t start, finish;   // clock_t 长整型数
    double    duration;

    start = clock();  // 返回程序开始执行后所用的时间 单位不为秒
    file_read_ht(); // 读取文件，创建散列表
    file_write_ht();  // 将散列表写入文件
    finish = clock();

    duration = (double)(finish - start) / CLOCKS_PER_SEC; // CLOCKS_PER_SEC等于每秒钟包含的系统时间单位数
    printf("%f seconds\n", duration);

    search_ht();    // 搜索功能

    return 0;
}
