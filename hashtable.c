#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "hashtable.h"

#define MAX_BUCKETS 1000
#define MULTIPLIER 31
#define TableSize 1000
#define Base 35

#define LENGTH 30
typedef struct User_login {
    char name[LENGTH]; // 姓名 
    int totalcount; // 登录次数
}ELementType;                 // 链表的节点

typedef struct SListNode
{
    ELementType data;
    struct SListNode* next;
}Node, * PNode, * List;              //封装链表节点和next指针


static PNode table[MAX_BUCKETS];

static unsigned long hashstring(const char* str);
static void cleartable();
static PNode walloc(const char* str);
static PNode lookup(const char* str);
static PNode find(PNode wp, const char* str);

/*创建一个节点*/
static PNode walloc(const char* str)
{
    PNode p = (PNode)malloc(sizeof(Node));
    if (p != NULL) {
        strcpy(p->data.name, str);
        p->data.totalcount = 0;
        p->next = NULL;
    }
    return p;
}

/**************************************************/
/*计算哈希值*/
static unsigned long hashstring(const char* str)
{
    unsigned long hash = 0;
    unsigned long k = 0, n = strlen(str) - 1;
    for (; k <= n; k++) {
        hash += str[k] * (unsigned long)pow((double)Base, (double)k);
    }
    return (hash % TableSize);
}

/**************************************************/
/*在一个链表中查找人名，找到返回指针，找不到返回NULL*/
static PNode find(PNode wp, const char* str)
{
    while (wp) {
        if (!strcmp(wp->data.name, str))
            return wp;
        wp = wp->next;
    }
    return NULL;
}

/**************************************************/
/*将在散列表中查找相应节点，并进行相应操作，找到返回指针，没找到则创建节点并加入散列表,并返回指针*/
static PNode lookup(const char* str)
{
    PNode p = find(table[hashstring(str)], str);
    if (p) {
        p->data.totalcount++;         // 找到str 登陆次数+1
        return p;
    }

    PNode pnew = walloc(str);
    if (!table[hashstring(str)]) {
        table[hashstring(str)] = pnew;
    }
    else {
        p = table[hashstring(str)];
        while (p->next)    p = p->next;
        p->next = pnew;
    }
    return pnew;
}

/*删除散列表*/
static void cleartable()
{
    PNode wp = NULL, p = NULL;
    int i = 0;

    for (i = 0; i < MAX_BUCKETS; i++) {
        wp = table[i];
        while (wp) {
            p = wp;
            wp = wp->next;
            free(p);
        }
    }
}

/**************************************************/
/*读取文件，创建散列表*/
void file_read_ht()
{
    FILE* fp = fopen("user_login.txt", "r");
    char word[1024];
    char* name;
    PNode wp = NULL;

    memset(table, 0, sizeof(table));

    int count = 0;
    while (1) {
        if (fscanf(fp, "%s", word) != 1)  // fscanf正常情况下返回从文件中读出的参数个数
            break;
        name = strtok(word, ","); // 获取,之前的字符串 即人名
        ////begin
        //加入散列表，2条语句实现
        if (!table[hashstring(name)]) {
            table[hashstring(name)] = walloc(name);
            table[hashstring(name)]->data.totalcount++;
        }
        else {
            lookup(name);
        }
        ////end

        count++;
    }
    printf("%d \n", count);
    fclose(fp);
}

/**************************************************/
/*将散列表写入文件*/
void file_write_ht()
{
    FILE* fp;
    int count = 0;

    if ((fp = fopen("output.txt", "wt")) == NULL) {
        printf("Fail to open file!\n");
        exit(0);
    }

    ////begin
    for (int i = 0; i < MAX_BUCKETS; i++)
    {
        PNode p = table[i];
        while (p) {
            fprintf(fp, "%s,%d\n", p->data.name, p->data.totalcount); // fprintf正常情况下返回写入文件的字节数
            p = p->next;
            count++;
        }
        free(p);
    }
    fclose(fp);

    ////end
    printf("%d\n", count);
}

/**************************************************/
/*搜索功能*/
void search_ht()
{
    char name[LENGTH];
    printf("Enter name, 'q' to exit：\n");
    scanf("%s", name);

    while (strcmp(name, "q")) {
        unsigned long hash = hashstring(name);
        PNode wp = table[hash];
        PNode curr = NULL;

        ////begin
        PNode p = find(wp, name);
        if (p)   printf("%s,%d\n", p->data.name, p->data.totalcount);
        else printf("Not exit\n");
        ////end

        scanf("%s", name);
    }

    cleartable();
}
